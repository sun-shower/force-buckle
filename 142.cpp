/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        set<ListNode*> a;
        //感觉这个比快慢指针好理解，复杂度也不高
        int size=0;
        while(head){
            a.insert(head);
            size++;
            if(a.size()<size)
            break;
            head = head->next;
        }
        if(head)
        return head;
        else
        return nullptr;
    }
};