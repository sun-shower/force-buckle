#define E 20
int i=2;
typedef struct {
    int *data;
    int top;
    int capacity;
} MyStack;


MyStack* myStackCreate() {
    MyStack *p = (MyStack*)malloc(sizeof(MyStack));
    p->data = (int*)malloc(sizeof(int)*E);
    p->capacity = E;
    top = -1;
    return p;
}

void myStackPush(MyStack* obj, int x) {
    obj->top++;
    if(obj->top >= obj->capacity){
        obj->data = realloc(obj->data,sizeof(int)*E*i);
        obj->capacity = E*i;
        i++;
    }
    obj->data[top] = x;
}

int myStackPop(MyStack* obj) {
    obj->top--;
    return obj->data[obj->top+1];
}

int myStackTop(MyStack* obj) {
    return obj->data[obj->top];
}

bool myStackEmpty(MyStack* obj) {
    if(obj->top==-1)
    return 1;
    
    return 0;
}

void myStackFree(MyStack* obj) {
    obj->top=-1;
}

/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);
 
 * int param_2 = myStackPop(obj);
 
 * int param_3 = myStackTop(obj);
 
 * bool param_4 = myStackEmpty(obj);
 
 * myStackFree(obj);
*/