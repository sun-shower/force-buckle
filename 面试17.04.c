int missingNumber(int* nums, int numsSize){
    int a=0,i;
    for(i=0; i<=numsSize; i++){
        a^=i;
    }
    for(i=0; i<numsSize; i++){
        a^=nums[i];
    }
    return a;
}