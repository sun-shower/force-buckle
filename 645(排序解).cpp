/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* findErrorNums(int* nums, int numsSize, int* returnSize){
    int i,j,t;
    int *res=malloc(sizeof(int)*2);
    *returnSize=2;
    for(i=0; i<numsSize; i++){
        for(j=0; j<numsSize-i-1; j++){
            if(nums[j]>nums[j+1]){
            t=nums[j];
            nums[j]=nums[j+1];
            nums[j+1]=t;
            }
        }
    }
    int curr,prev=0;
    for (i = 0; i < numsSize; i++) {
            curr = nums[i];
            if (curr == prev) {
                res[0] = prev;
            } else if (curr - prev > 1) {
                res[1] = prev + 1;
            }
            prev = curr;
        }
        if (nums[numsSize - 1] != numsSize) {
            res[1] = numsSize;
        }
        return res;

}