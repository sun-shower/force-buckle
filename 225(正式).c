typedef int QDateType; // 队列存储数据类型

typedef struct QueueNode // 队列元素节点
{
    QDateType val;
    struct QueueNode *next;
} QueueNode;

typedef struct Queue // 队列
{
    QueueNode *head;
    QueueNode *end;
    int size;
} Queue;

void QueueInti(Queue **qqu)
{
    *qqu = (Queue *)malloc(sizeof(Queue));
    (*qqu)->end = (*qqu)->head = NULL;
    (*qqu)->size = 0;
}
// 队列初始化
void QueueDestory(Queue *qu)
{
    assert(qu);
    QueueNode *cur, *t;
    cur = qu->head;
    while (cur)
    {
        t = cur;
        cur = cur->next;
        free(t);
    }
    qu->head = qu->end = NULL;
    qu->size = 0;
}
// 队列的销毁
void QueuePush(Queue *qu, QDateType x)
{
    assert(qu);
    QueueNode *p = (QueueNode *)malloc(sizeof(QueueNode));
    p->val = x;
    p->next = NULL;
    if (qu->end == NULL)
    {
        qu->head = qu->end = p;
    }
    else
    {
        qu->end->next = p;
        qu->end = p;
    }
    qu->size++;
}
// 入队
void QueuePop(Queue *qu)
{
    assert(qu);
    QueueNode *next;
    assert(qu->head);
    next = qu->head->next;
    if (next == NULL)
    {
        free(qu->head);
        qu->head = qu->end = NULL;
    }
    else
    {
        free(qu->head);
        qu->head = next;
    }
    qu->size--;
}
// 出队
QDateType QueueFront(Queue *qu)
{
    assert(qu);
    assert(qu->head);
    return qu->head->val;
}
// 取出队首元素
int QueueSize(Queue *qu)
{
    return qu->size;
}
// 求队列的长度
bool QueueEmpty(Queue *qu)
{
    if (qu->size == 0)
        return true;
    else
        return false;
}
// 判断队是否为空

typedef struct
{
    Queue *a;
    int size;
} MyStack;

MyStack *myStackCreate()
{
    MyStack *p = (MyStack *)malloc(sizeof(MyStack));
    QueueInti(&(p->a));
    p->size = 0;
    return p;
}

void myStackPush(MyStack *obj, int x)
{
    obj->size++;
    QueuePush(obj->a, x);
}

int myStackPop(MyStack *obj)
{
    int size = obj->a->size - 1;
    while (size--)
    {
        int n = QueueFront(obj->a);
        QueuePush(obj->a, n);
        QueuePop(obj->a);
    }
    int i = QueueFront(obj->a);
    QueuePop(obj->a);
    obj->size--;
    return i;
}

int myStackTop(MyStack *obj)
{
    int size = obj->a->size - 1, i;
    while (size--)
    {
        i = QueueFront(obj->a);
        QueuePush(obj->a, i);
        QueuePop(obj->a);
    }
    i = QueueFront(obj->a);
    QueuePush(obj->a, i);
    QueuePop(obj->a);
    return i;
}

bool myStackEmpty(MyStack *obj)
{
    if (obj->size == 0)
        return 1;
    else
        return 0;
}

void myStackFree(MyStack *obj)
{
    QueueDestory(obj->a);
    obj->size = 0;
}

/**
 * Your MyStack struct will be instantiated and called as such:
 * MyStack* obj = myStackCreate();
 * myStackPush(obj, x);

 * int param_2 = myStackPop(obj);

 * int param_3 = myStackTop(obj);

 * bool param_4 = myStackEmpty(obj);

 * myStackFree(obj);
*/