class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        
       if(nums.size()==0)
        return vector<int>{-1, -1};
        int first,end;
        first = f(nums,target);
        end = s(nums,target);

        return vector<int>{first, end};
    }
private:
    int f(vector<int>& nums, int target){
        int l = 0,r = nums.size()-1;
        int mid;
        while(l<r){
            mid = (r+l)/2;
            if(nums[mid]<target)
            l=mid+1;
            else if(nums[mid]==target)
            r=mid;
            else
            r=mid-1;
        }
        if(l==r&&nums[l]==target)
        return l;

        return -1;
    }
    int s(vector<int>& nums, int target){
        int l = 0,r = nums.size()-1;
        int mid;
        while(l<r){
            mid = (r+l+1)/2;
            if(nums[mid]<target)
            l=mid+1;
            else if(nums[mid]==target)
            l=mid;
            else
            r=mid-1;
        }
        if(l==r&&nums[l]==target)
        return l;
    
        return -1;
    }
};