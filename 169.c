int majorityElement(int *nums, int numsSize)
{

    int t = nums[0], c = 1;

    int i;
    for (i = 1; i < numsSize; i++)
    {

        if (nums[i] == t)
            c++;
        else
            c--;
        if (!c)
            t = nums[i], c = 1;
    }
    return t;
}