class Solution {
public:
    bool isHappy(int n) {
        int left,right;

        left = next(n); 
        right = next(left);
        while(left!=1 && right!=1 && left!=right){
            left = next(left);
            right = next(right);
            right = next(right);
        }
        if(left==1||right==1)
        return true;
        
        return false;
    }
    int next(int n){
        int sum = 0,t;
        while(n!=0){
            t = (n%10);
            sum += t*t;
            n/=10;
        }

        return sum;
    }
};