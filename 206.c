/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

struct ListNode* reverseList(struct ListNode* head){
    struct ListNode *p1,*p2,*next;
    p1=head;p2=NULL;
    while(p1){
        next = p1->next;
        p1->next=p2;
        p2=p1;
        p1=next;
    }
    return p2;
}