class Solution {
public:
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
        vector<int> a;
        int j=0;

    for(int i=0; i<pushed.size(); i++){
        a.push_back(pushed[i]);
        while(!a.empty()&&*(a.end()-1)==popped[j]){
            j++;
            a.pop_back();
        }
    }
    if(a.empty())
    return true;

    return false;
    }
};