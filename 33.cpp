class Solution
{
public:
    int search(vector<int> &nums, int target)
    {
        int l = 0, r = nums.size();
        int mid;
        if (nums[0] > target)
            while (l < r)
            {
                mid = (l + r) / 2;
                if (nums[mid] > nums[r])
                    l = mid + 1;
            }
        else if (nums[0] < target)
        {
            if (nums[0] > target)
                while (l < r)
                {
                    mid = (l + r) / 2;
                    if (nums[mid] < nums[0])
                        r = mid + 1;
                }
        }
        while (l < r)
        {
            mid = (l + r) / 2;
            if (nums[mid] < target)
            {
                l = mid + 1;
            }
            else if (nums[mid] > target)
            {
                r = mid - 1;
            }
            else
            return mid;
        }

        return mid;
    }
};