/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        int n=1;
        ListNode* p=head;
        while(p->next!=NULL){
            p=p->next;
            n++;
        }
        if(n%2==0){
            n/=2;
            while(n--){
                head=head->next;
            }
        }
        else{
            n=(n-1)/2;
            while(n--){
                head=head->next;
            }
        }
        return head;
    }
};