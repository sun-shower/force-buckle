/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        ListNode* p=head;
        int c=1;
        while(p->next){
            c++;
            p=p->next;
        }
        p=head;
        n=c-n-1;
        if(n<0)
        return head ->next;
        for(int i=0; i<n; i++){
            p=p->next;
        }
        //ListNode* t=p;
        if(p->next){
        p->next=(p->next)->next;
        return head;
        }
        else
        return NULL;
    }
};