#define E 20
typedef struct {
    int *data;
    int top;
    int capacity;
} Stack;


Stack* StackCreate() {
    Stack *p = (Stack*)malloc(sizeof(Stack));
    p->data = (int*)malloc(sizeof(int)*E);
    p->capacity = E;
    p->top = -1;
    return p;
}

void StackPush(Stack* obj, int x) {
    obj->top++;
    if(obj->top >= obj->capacity){
        obj->data = realloc(obj->data,obj->capacity*2);
        obj->capacity = obj->capacity*2;
    }
    obj->data[obj->top] = x;
}

int StackPop(Stack* obj) {
    obj->top--;
    return obj->data[obj->top+1];
}

int StackTop(Stack* obj) {
    return obj->data[obj->top];
}

bool StackEmpty(Stack* obj) {
    if(obj->top==-1)
    return 1;
    
    return 0;
}

void StackFree(Stack* obj) {
    obj->top=-1;
}
////////////////////////////////////�ָ���//////////////////////////
typedef struct {
    Stack *a;
    Stack *b;
    int size;
} MyQueue;


MyQueue* myQueueCreate() {
    MyQueue *p = (MyQueue*)malloc(sizeof(MyQueue));
    p->a = StackCreate();
    p->b = StackCreate();
    p->size = 0;
    return p;
}

void myQueuePush(MyQueue* obj, int x) {
    StackPush(obj->a,x);
    obj->size++;
}

int myQueuePop(MyQueue* obj) {
    if(StackEmpty(obj->b))
    while(obj->a->top!=-1){
        StackPush(obj->b,StackPop(obj->a));
    }
    obj->size--;
    return StackPop(obj->b);
}

int myQueuePeek(MyQueue* obj) {
    if(StackEmpty(obj->b))
    while(obj->a->top!=-1){
        StackPush(obj->b,StackPop(obj->a));
    }
    return StackTop(obj->b);
}

bool myQueueEmpty(MyQueue* obj) {
    if(obj->size==0)
    return 1;
    else
    return 0;
}

void myQueueFree(MyQueue* obj) {
    StackFree(obj->a);
    StackFree(obj->b);
    obj->size = 0;    
}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);
 
 * int param_2 = myQueuePop(obj);
 
 * int param_3 = myQueuePeek(obj);
 
 * bool param_4 = myQueueEmpty(obj);
 
 * myQueueFree(obj);
*/