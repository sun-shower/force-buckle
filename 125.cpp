class Solution {
public:
    bool isPalindrome(string s) {
        string b = "";
        for(auto a:s){
            if((a>='a'&&a<='z')||(a>='A'&&a<='Z')||(a>='0'&&a<='9'))
            b+=toupper(a);
        }
        string _b(b.rbegin(),b.rend());
       return b==_b;
    }
};