class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int n=0,t;
        for(int i=0; i<32; i++){
            t=0;
            for(int a:nums){
                t+=(a>>i)&1;
            }
            n+=(t%3)<<i;
            
        }
        return n;
    }
};