/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int zc(int n)
{
    int i = 1;
    int m = n;
    while (m)
    {
        i = m % 10;
        if (i && n % i)
            return 0;
        else if (!i)
        {
            return 0;
        }
        m /= 10;
    }
    return 1;
}
int *selfDividingNumbers(int left, int right, int *returnSize)
{
    int count = 0;
    int *p = (int *)malloc(sizeof(int) * (right - left + 1));
    int i;
    for (i = left; i <= right; i++)
    {
        if (zc(i))
        {
            p[count] = i;
            count++;
        }
    }
    *returnSize = count;
    return p;
}
