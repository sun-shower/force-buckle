/**
 * Note: The returned array must be malloced, assume caller calls free().
 */

int *findDisappearedNumbers(int *nums, int numsSize, int *returnSize)
{
    int i, c = 0;
    for (i = 0; i < numsSize; i++)
    {
        if (nums[i] % numsSize)
            nums[nums[i] % numsSize - 1] += numsSize;
        else
            nums[numsSize - 1] += numsSize;
    }
    *returnSize = 0;
    for (i = 0; i < numsSize; i++)
    {
        if (nums[i] <= numsSize)
        {
            *returnSize += 1;
            nums[c++] = i + 1;
        }
    }
    return nums;
}