class Solution {
public:
    int dominantIndex(vector<int>& nums) {
        int n=nums.size();
        if(n==1)
        return 0;
        int max=0,cmax=0,t=-1;
        for(int i=0; i<n; i++){
            if(nums[i]>max){
            cmax=max;
            max=nums[i];
            t=i;
            }
            else if (nums[i] > cmax) {
                cmax = nums[i];
            }
        }
        if(max>=2*cmax)
        return t;

        return -1;
    }
};