/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        set<ListNode*> a;
        int size = 0; 
        while(head){
            a.insert(head);
            size++;
            if(a.size()<size)
            return true;;
            head = head->next;
        }
        return false;
    }
};