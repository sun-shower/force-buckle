/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* productExceptSelf(int* nums, int numsSize, int* returnSize){
    int *arr = (int*)malloc(sizeof(int)*numsSize);
    int i;
    arr[0] = 1;
    arr[1] = 1;
    for(i=1; i<numsSize; i++){
        arr[i] = nums[i-1]*arr[i-1]; 
    }
    int t=1;

    for(i=numsSize-2; i>=0; i--){
        t = nums[i+1]*t;
        arr[i] *= t;
    }
    *returnSize = numsSize;
    return arr;
}