class Solution {
public:
    void reverseString(vector<char>& s) {
        int l=0,r=s.size()-1;
        char t;
        while(l<r)
        {
            t=s[l];
            s[l]=s[r];
            s[r]=t;
            l++,r--;
        }

    }
};