class Solution {
public:
    string addStrings(string num1, string num2) {
        string a = "";
        int i=num1.size()-1,j = num2.size()-1;
        int n,k=0;
        while(i>=0||j>=0){
            if(i>=0&&j>=0){
                n = (num1[i]-'0')+(num2[j]-'0')+k;
                k = n/10;
                n = n%10;
                a += (char)(n+'0');
                i--,j--;
            }
            else if(i>=0){
                n = num1[i]-'0' + k;
                k = n/10;
                n = n%10;
                a += (char)(n+'0');
                i--;
            }
            else{
                n = num2[j]-'0' + k;
                k = n/10;
                n = n%10;
                a += (char)(n+'0'); 
                j--;
            }
        }
        if(k>0){
            a += (char)(k+'0'); 
        }
        string b(a.rbegin(),a.rend());
        return b;
    }
};