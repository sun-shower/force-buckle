class Solution {
public:
    string reverseStr(string s, int k) {
        string::iterator l;
        l = s.begin();
        while(l+2*k<s.end()){
            reverse(l,l+k);
            l+=2*k;
        }
        if(l+k<s.end())
        reverse(l,l+k);
        else
        reverse(l,s.end());
        return s;
    }
};8