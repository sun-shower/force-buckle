class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        int sum=0;
        for(int a:nums){
            sum+=a;
        }
        int n=nums.size();

        int m=0;
        for(int i=0; i<n; i++){
            
            if(sum-nums[i]==m*2)
            {
                return i;
            }
            m+=nums[i];
        }
        return -1;
    }
};