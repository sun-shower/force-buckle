class Solution {
public:
    TreeNode* re=nullptr;
    bool dfs(TreeNode* root, TreeNode* p, TreeNode* q){
        if(root==nullptr||re!=nullptr)//已经找到就不找了
        return false;


        bool left = dfs(root->left,p,q);
        bool right = dfs(root->right,p,q);


        if((left&&right)||((root==p||root==q)&&(left||right))){
        re = root;
        return true;
        }
        if(left||right||root==p||root==q)
        return true;


        return false;
    }
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        dfs(root,p,q);


        return re;
    }
};