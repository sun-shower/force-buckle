/**
 * Note: The returned array must be malloced, assume caller calls free().
 */

int *masterMind(char *solution, char *guess, int *returnSize)
{
    int a[4] = {0}, b[4] = {0};
    int *p = malloc(sizeof(int) * 2);
    *returnSize = 2;
    int i;
    for (i = 0; i < 4; i++)
    {
        switch (solution[i])
        {
        case 'R':
            a[0]++;
            break;
        case 'G':
            a[1]++;
            break;
        case 'B':
            a[2]++;
            break;
        case 'Y':
            a[3]++;
            break;
        }
    }
    for (i = 0; i < 4; i++)
    {
        switch (guess[i])
        {
        case 'R':
            b[0]++;
            break;
        case 'G':
            b[1]++;
            break;
        case 'B':
            b[2]++;
            break;
        case 'Y':
            b[3]++;
            break;
        }
    }
    int m, n;
    m = n = 0;
    for (i = 0; i < 4; i++)
    {
        m += a[i] < b[i] ? a[i] : b[i];
    }
    for (i = 0; i < 4; i++)
    {
        if (solution[i] == guess[i])
            n++;
    }
    p[0] = n;
    p[1] = m - n;
    return p;
}