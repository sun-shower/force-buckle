class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int a = 0;

        for(auto b: nums){
            a ^= b;
        }

        return a;
    }
};